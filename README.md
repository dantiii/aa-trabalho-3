# Algoritmos Avançados | Trabalho 3 - Hipótese A-1 – Strings de letras minúsculas

Pretende-se que estude e desenvolva o **"Frequent Count - Algorithm Misra & Gries"** para determinar as letras mais frequentes de um conjunto de caracteres aleatórios.

Docente: Joaquim Madeira

Aluno: Dante Marinho (83672)

## Como Correr o Programa

Digitar o seguinte comando na linha de comandos:

python frequent_count.py

## Observações

### No topo do script poderá alterar as seguintes variáveis de configurações:

- is_mode_generate_dataset_files: True para gerar os datasets ou False para apenas correr o programa depois de já ter gerado os datasets desejados
- output_length_list: lista para indicar o tamanho de cada ficheiro (dataset), onde o número de itens (apenas números) da lista será a quantidade de ficheiros gerados

### Variáveis do programa:

- k :: número de contadores => variável de entrada utilizada na função do Algoritmo de Misra e Gries
- output_length_actual :: tamanho do dataset que deseja utilizar numa execução do programa
- folder :: nome da pasta para armazenar os datasets gerados a partir do alfabeto
- output_path_actual :: caminho completo para o ficheiro contendo o dataset 
- alphabet :: lista contendo um alfabeto onde poderá atribuir diferentes probabilidades a diferentes letras

## Link deste repositório

https://gitlab.com/dantiii/aa-trabalho-3