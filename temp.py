import random

alphabet = ['a', 'b', 'c', 'd', 'e', 'f']

if len(alphabet) > 0:
    print('É maior que zero.')

A = {'a': 2, 'b': 5}
del(A['a'])
print(A)

abc = 'abcdef'
choice = random.choice(abc)
print('Abc:', choice)

choice = random.choice(alphabet)
print('alphabet:', choice)


import os

writepath = 'some/path/to/file.txt'

mode = 'a' if os.path.exists(writepath) else 'w'
with open(writepath, mode) as f:
    f.write('Hello, world!\n')

