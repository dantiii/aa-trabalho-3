import random
import datetime
import os
import psutil

# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
# Algoritmos Avançados (2019/2020)
# Trabalho 3 - Frequent Count
# Prof.: Joaquim Madeira
# Aluno: Dante Marinho
#
# Developed with Python 3.7.4
# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

# --- Config variables ------------------------------------------------------------------

# Put = True to generate a folder with dataset files
is_mode_generate_dataset_files = False

# List of files(with N chars) to generate inside the folder
output_length_list = [100, 1000, 10000, 100000, 1000000]  # number of characters in output file (test file)

# -------------------------------------------------------------------------------


# --- Variables ------------------------------------------------------------------

# Get process ID to measure memory
process = psutil.Process(os.getpid())

k = 5  # Number of counters :: Input variable for the Misra & Gries Algorithm function
output_length_actual = 1000  # length of a existing dataset 

# --- Vars to generate different probability dataset
folder = 'dataset-different-prob/'  # Folder to store dataset generated from the alphabet with different probability
alphabet = 'aaabcdeeeeeeefghiiiiiiiiiiijklmnooooooooooooooopqrstuuuuuuuuuuuuuuuuuuuuvwxyz'  # Alphabet with higher probability to some of the letters (vowels) => different prob

# --- Vars to generate equivalent probability dataset
# folder = 'dataset-equivalent-prob/'  # Folder to store dataset generated from the alphabet with equivalent probability
# alphabet = 'abcdefghijklmnopqrstuvwxyz'  # Alphabet with equivalent probability for all letters

output_path_actual = folder + 'letters_' + str(output_length_actual) + '.txt'

# -------------------------------------------------------------------------------


print('\n~ ~ ~ [k = {}] ~ ~ ~'.format(k))


# -------------------------------------------------------------------------------
# This function generates various output files with N = 100, 1000, 10000... chars
# -------------------------------------------------------------------------------
def generate_test_files():
    for length in output_length_list:
        output_path = folder + 'letters_' + str(length) + '.txt'
        os.makedirs(folder, exist_ok=True)  # succeeds even if directory exists.
        with open(output_path, 'w') as output_file:
            for i in range(length):
                choice = random.choice(alphabet)
                if i < length - 1:  # treatement for the last position 
                    output_file.write(choice + ' ')
                else:
                    output_file.write(choice)  # last position is saved without space
            output_file.close()
if is_mode_generate_dataset_files:
    generate_test_files() # geretaing test file (uncomment this line after generate all needed dataset files)


# -------------------------------------------------------------------------------
# Returns the dataset :: The content (random letters) in one test file
# -------------------------------------------------------------------------------
def get_output():

    if not os.path.exists(output_path_actual):
        print('(!) There is no dataset with length of {}. Generate this dataset and try again.'.format(output_length_actual))
        exit()  # Exit script
    else:
        output_file = open(output_path_actual, 'r')
        output = output_file.readline()
        return output.split(' ')


# Variable with the sequence already splited by space => The dataset
sequence = get_output()  # Dataset


# -------------------------------------------------------------------------------
# Exact counter
# -------------------------------------------------------------------------------
counter = {}
def exact_counter():
    print('\n~ ~ ~ Exact Counter #{} ~ ~ ~\n'.format(output_length_actual))
    print('Letter   #      %\n--------------------')
    for char in sequence:
        if char in counter:
            counter[char] += 1
        else:
            counter[char] = 1

    # Sorted by value
    # quantity = 0
    for key, value in sorted(counter.items(), key=lambda item: item[1], reverse=True):
        print("{}        {:<5}  {:<4}%".format(key, value, round(value / output_length_actual * 100, 1)))
        # quantity += 1
        # if quantity == 15:  # Condition just to show less results
        #     break
  
start_time = datetime.datetime.now()
exact_counter()  # Call for exact coiunter function
print('Total time for exact counter:', datetime.datetime.now() - start_time)


# -------------------------------------------------------------------------------
# Algorithm for determining the most estimated frequently letters
# -------------------------------------------------------------------------------
def misra_gries_algorithm(k, sequence):

    A = {}  # Array containing up to "k - 1" pairs of counters ("value": "approximate frequency of value")

    for letter in sequence:
        if letter in A:
            A[letter] += 1  # Increment the counter for this letter, if already exists
        elif len(A) < k - 1:
            A[letter] = 1  # Create the new counter for this letter, if not exists
        else:
            for key in list(A):  # Converting "dict" to "list" becouse is don't possible change the len(<dict>) during iteration
                A[key] = A[key] - 1
                if A[key] == 0:
                    del(A[key])
    return A

start_time = datetime.datetime.now()
A = misra_gries_algorithm(k, sequence)  # Call for Misra-Gries Algorithm function

print('\n~ ~ ~ Output Misra & Gries #{} ~ ~ ~\n'.format(output_length_actual))
print('Letter   #\n------------------')

# Output "A" sorted by value
for key, value in sorted(A.items(), key=lambda item: item[1], reverse=True):
    print("%s        %s" % (key, value))
print('Num contadores:', len(A))
print('Total time for Misra-Gries:', datetime.datetime.now() - start_time)
print()

print('Foi utilizado um total de {} MB.'.format(round((process.memory_info().rss / 1024 / 1024), 3)))


# --- End of the script ---

# -------------------------------------------------------------------------------
# Help Links
# -------------------------------------------------------------------------------

# How to avoid “RuntimeError: dictionary changed size during iteration” error?
# https://stackoverflow.com/questions/11941817/how-to-avoid-runtimeerror-dictionary-changed-size-during-iteration-error

# How to sort a Python dict (dictionary) by keys or values
# https://www.saltycrane.com/blog/2007/09/how-to-sort-python-dictionary-by-keys/

# How can I safely create a nested directory? (create foder if not exsists)
# https://stackoverflow.com/questions/273192/how-can-i-safely-create-a-nested-directory
# https://www.tutorialspoint.com/How-can-I-create-a-directory-if-it-does-not-exist-using-Python

# How to left align a fixed width string?
# https://stackoverflow.com/questions/12684368/how-to-left-align-a-fixed-width-string

# Total memory used by Python process?
# https://stackoverflow.com/questions/938733/total-memory-used-by-python-process